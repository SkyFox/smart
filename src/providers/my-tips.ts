import { Injectable } from '@angular/core';
import { AlertController,LoadingController,ToastController  } from 'ionic-angular';

@Injectable()
export class MyTips {

  constructor(public alertCtrl: AlertController,
                      public loadingCtrl: LoadingController,
                      public toastCtrl: ToastController ) {
  }

//Alert组件，参数为 标题 、副标题、按钮文字
  showAlert(title,subTitle,buttons) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: [buttons]
    });
    alert.present();
  }

//Loading组件，参数为 形状 、文字
  presentLoading(spinner,content) {
    let loader = this.loadingCtrl.create({
      spinner:spinner,
      content: content
    });
    return loader;
  }

//Toast组件，参数为 信息 、展示时间、展示位置
  presentToast(message,duration,position) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });
    toast.present();
  }

}
