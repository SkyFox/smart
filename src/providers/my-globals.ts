import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the MyGlobals provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MyGlobals {

  public myPosition = [];

  constructor(public http: Http) {
    console.log('Hello MyGlobals Provider');
  }

  setMyPoistion(long,lat){
      this.myPosition[0] = long;
      this.myPosition[1] =lat;
  }

  getMyPosition(){
      return this.myPosition;
  }

}
