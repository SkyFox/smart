import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';
import { MyHttp } from './../../providers/my-http';

/*
  Generated class for the TransactionDetails page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-transaction-details',
  templateUrl: 'transaction-details.html'
})
export class TransactionDetailsPage {

  public userInfo:any;
  public transactions = [];
  constructor(public navCtrl: NavController, public navParams: NavParams,
   public myTips: MyTips,
   public myStoarge: MyStoarge,
   public myHttp: MyHttp,) {
         this.userInfo = this.myStoarge.getObject("user_info");
         this.getTransactionList();
   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionDetailsPage');
  }

  getTransactionList(){
    this.myHttp.post('service/findAccountDetail',{'mobile':this.userInfo.mobile}).subscribe(
      data =>{
           if(data.hasOwnProperty("msg")){
              this.myTips.presentToast(data.msg,2000,"bottom")
           }else{
              this.transactions = data;
              this.transactions.forEach(item=>{
                  if(item.type=="01"){
                      item.tranType='支付宝充值';
                      item.money="+"+item.money;
                  }else if(item.type=="02"){
                      item.tranType="充电消费";
                      item.money="-"+item.money;
                  }else if(item.type=="03"){
                      item.tranType="账户退款";
                      item.money="-"+item.money;
                  }else if(item.type=='00'){
                      item.tranType="微信充值";
                      item.money="-"+item.money;
                  }
              });
           }
            
      },
      error =>{
          this.myTips.presentToast("服务器异常！",2000,"bottom");
      } 
    )
  }

}
