import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';


/*
  Generated class for the Recharge page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-recharge',
  templateUrl: 'recharge.html'
})
export class RechargePage {

  public pattern = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
  public payType='alipay';
  public money:any;
  public userInfo:any;

  public callBack:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public alertCtrl:AlertController,
  public myHttp: MyHttp,
  public myTips: MyTips,
  public myStoarge: MyStoarge, ) {
        this.userInfo = this.myStoarge.getObject("user_info");
        this.callBack = navParams.get("callBack")||'';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RechargePage');
  }

  _keyPress(event: any) {
    const pattern = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
   // let inputChar = String.fromCharCode(event.charCode);

    if (pattern.test(this.money)) {
      // invalid character, prevent input
      event.preventDefault();
    }
}

  confirm(){

     if(!this.pattern.test(this.money)){
        this.myTips.presentToast("金额输入有误，请重新输入！",2000,"bottom")
        return;
     }


      let confirm = this.alertCtrl.create({
        title: '确认支付',
        message: this.money+'(元)',
        buttons: [
          {
            text: '取消',
            handler: () => {
            }
          },
          { 
            text: '确定',
            handler: () => {
                  if(this.payType=='alipay'){
                      this.apliyPay();
                  }else{
                      this.wechatPay();
                  }
            }
          }
        ]
    });
    confirm.present();
  }

  apliyPay(){
      let myPresent= this.myTips.presentLoading("ios-small","支付中...");
      myPresent.present();
      let params = "";
      this.myHttp.post('pay/alipayEncrypt',{"money":this.money,"mobile":this.userInfo.mobile}).subscribe(
                  data =>{
                      myPresent.dismiss();
                      if(data.sucess===true){
                         params = data.msg
                         Alipay.Base.pay(params,(data)=>{
                             if(data.resultStatus==9000){
                                 let alert = this.alertCtrl.create({
                                    title: '提示',
                                    subTitle: '支付成功',
                                    buttons: [{
                                      text: '确定',
                                      handler : ()=>{
                                          if(this.callBack){
                                            this.callBack().then(()=>{
                                                  this.navCtrl.pop();
                                            })
                                          }else{
                                            this.navCtrl.pop();
                                          }
                                      }}]
                                  });
                                  alert.present();
                             }
                             
                              },(data)=>{
                                  let alert = this.alertCtrl.create({
                                  title: '提示',
                                  subTitle: '支付失败',
                                  buttons: ['确定']
                                });
                                alert.present();
                          })
                      }else{
                        this.myTips.presentToast(data.msg,2000,"bottom");
                      }
                  },
                  error =>{
                      myPresent.dismiss();
                      this.myTips.presentToast("服务器异常！",2000,"bottom");
                  } 
                )
  }

sendPaymentRequest(params: any) {  
    return new Promise((resolve, reject) => {  
      Wechat.sendPaymentRequest(params, result => {  
        resolve(result);  
      }, error => {  
        reject(error);  
      });  
    });  
  }  

  wechatPay(){
      let myPresent= this.myTips.presentLoading("ios-small","支付中...");
      myPresent.present();
      this.myHttp.post('pay/weixinPrePay',{"money":this.money,"mobile":this.userInfo.mobile}).subscribe(
                  data =>{
                      myPresent.dismiss();
                      let params={
                         mch_id : data.partnerid,
                         prepay_id : data.prepayid,
                         nonce : data.noncestr,
                         timestamp : data.timestamp,
                         sign : data.sign
                      }

                      this.sendPaymentRequest(params).then((result=>{
                          let alert = this.alertCtrl.create({
                            title: '提示',
                            subTitle:'支付成功',
                            buttons: [{
                                text: '确定',
                                handler : ()=>{
                                    if(this.callBack){
                                       this.callBack().then(()=>{
                                            this.navCtrl.pop();
                                       })
                                    }else{
                                      this.navCtrl.pop();
                                    }
                                }}]
                          });
                          alert.present();
                      }),
                      error=>{
                          let alert = this.alertCtrl.create({
                              title: '',
                              subTitle: '未支付',
                              buttons: ['确定']
                            });
                            alert.present();
                      })
                     // }else{
                     //   this.myTips.presentToast(data.msg,2000,"bottom");
                     // }
                  },
                  error =>{
                      myPresent.dismiss();
                      this.myTips.presentToast("服务器异常！",2000,"bottom");
                  } 
      )
      
  }

}
