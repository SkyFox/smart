import { Component } from '@angular/core';
import { NavController, NavParams,ViewController,AlertController,App} from 'ionic-angular';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';
import { MyHttp } from './../../providers/my-http';
import { TabsPage } from './../tabs/tabs';

/*
  Generated class for the UpdateUser page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-update-user',
  templateUrl: 'update-user.html'
})
export class UpdateUserPage {

  public userAlias:any;

  public carType:any;

  public userInfo:any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public viewCtrl: ViewController,
  public myTips: MyTips,
   public myStoarge: MyStoarge,
   public myHttp: MyHttp,
   public alertCtrl: AlertController,
   public app:App) {

          this.userInfo = this.myStoarge.getObject("user_info");

          this.carType = this.userInfo.carType;
          this.userAlias = this.userInfo.alias;



   }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateUserPage');
  }

   dismiss(){
        console.log("============length",);
        this.navCtrl.popAll();
    }

   update(){
    this.myHttp.post('service/modifyAppUserInfo',{'mobile':this.userInfo.mobile,'alias':this.userAlias,'carType':this.carType}).subscribe(
      data =>{
           if(data.sucess == true){
                let alert = this.alertCtrl.create({
                  title: '',
                  subTitle: '修改成功',
                  buttons: [
                    {text:'确定',
                     handler:()=>{
                        this.userInfo.carType = this.carType;
                        this.userInfo.alias = this.userAlias;
                        this.myStoarge.setObject("user_info",this.userInfo);
                        this.viewCtrl.dismiss();
                     }}]
                });
                alert.present();
           }else{
             this.myTips.presentToast(data.msg,2000,"bottom")
           }
            
      },
      error =>{
          this.myTips.presentToast("服务器异常！",2000,"bottom");
      } 
    )
   }

}
