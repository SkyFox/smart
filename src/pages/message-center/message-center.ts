import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';
import { MyHttp } from './../../providers/my-http';
import { MessageDetailPage } from './../message-detail/message-detail';

/*
  Generated class for the MessageCenter page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-message-center',
  templateUrl: 'message-center.html'
})
export class MessageCenterPage {

  public userInfo:any;
  public messages=[];
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public myTips: MyTips,
    public myStoarge: MyStoarge,
    public myHttp: MyHttp,) {
         this.userInfo = this.myStoarge.getObject("user_info");
         this.getMesssages();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessageCenterPage');
  }

  getMesssages(){
    this.myHttp.post('other/findMessages',{'mobile':this.userInfo.mobile}).subscribe(
      data =>{
            if(data){
                this.messages = data;
            }else{

            }
      },
      error =>{
          console.log(error);
          this.myTips.presentToast("服务器异常！",2000,"bottom")
      } 
    )
  }

  setIsRead(message):string{

      if(message.read==0){
          return "#eee";
      }else{
          return "#fff";
      }

  }

  goToMsgDetail(message){
      this.navCtrl.push(MessageDetailPage,message);
  }

  removeItem(message,msgIndex){
    this.myHttp.post('other/deleteMessage',{'mobile':this.userInfo.mobile,
      'messageId' : message.id}).subscribe(
          data =>{
              if(data.sucess === true){
                  this.messages.splice(msgIndex);
              }else{
                this.myTips.presentToast(data.msg,2000,"bottom")
              }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",2000,"bottom")
          } 
    );
  }

}
