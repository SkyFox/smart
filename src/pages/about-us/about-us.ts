import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';

/*
  Generated class for the AboutUs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html'
})
export class AboutUsPage {

  public aboutUs = {
     'wechat' : '',
     'contactNo' : '',
     'email' : '',
     'link' : ''
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,
            public myTips: MyTips,
            public myHttp: MyHttp,) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutUsPage');
    this.myHttp.post('other/aboutUs',null).subscribe(
      data =>{
          this.aboutUs.contactNo = data.c2;
          this.aboutUs.email = data.c3;
          this.aboutUs.link = data.c4;
          this.aboutUs.wechat = data.c1;
      },
      error =>{
          console.log(error);
          this.myTips.presentToast("服务器异常！",2000,"bottom")
      } 
    )
  }



}
