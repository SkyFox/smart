import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { RegNotesPage } from './../reg-notes/reg-notes';
import { PayNotesPage } from './../pay-notes/pay-notes';
import { UsageNotesPage } from './../usage-notes/usage-notes';

/*
  Generated class for the HelpCenter page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-help-center',
  templateUrl: 'help-center.html'
})
export class HelpCenterPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpCenterPage');
  }

  goToRegNotePage(){
      this.navCtrl.push(RegNotesPage);
  }

  goToUsagePage(){
      this.navCtrl.push(PayNotesPage);
  }

  goToPayPage(){
      this.navCtrl.push(UsageNotesPage);
  }

}
