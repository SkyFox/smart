import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';
import { MyHttp } from './../../providers/my-http';

/*
  Generated class for the MessageDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-message-detail',
  templateUrl: 'message-detail.html'
})
export class MessageDetailPage {

  public message:any;
  public userInfo:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
   public myTips: MyTips,
   public myStoarge: MyStoarge,
   public myHttp: MyHttp,) {
      this.message = this.navParams.data;
      this.userInfo = this.myStoarge.getObject("user_info");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessageDetailPage');
  }
  ionViewWillEnter(){
      this.setMessage2Read();
  }

  setMessage2Read(){
    this.myHttp.post('other/getReadMark',{'mobile':this.userInfo.mobile,'messageId':this.message.id}).subscribe(
      data =>{
           console.log(data);
      },
      error =>{
          console.log(error);
          this.myTips.presentToast("服务器异常！",2000,"bottom")
      } 
    )
  }

}
