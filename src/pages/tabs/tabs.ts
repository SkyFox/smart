import { Component } from '@angular/core';
import { NavController,NavParams,Events } from 'ionic-angular';

import { HomePage } from './../home/home';
import { MyCenterPage } from './../my-center/my-center';
import { QRCodePage } from './../qr-code/qr-code';
import {ServicesPage} from './../services/services';


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root: any = HomePage;
  tab2Root: any = QRCodePage;
//  tab3Root: any = ServicesPage;
  tab4Root: any = MyCenterPage;

  mySelectedIndex: number;

  constructor(public navCtrl: NavController,public navParams: NavParams,public event:Events) {
      console.log("-------------s---------");
  }

  ionViewDidLoad(){
     
  }

  ionViewDidEnter(){
      /*if(this.navParams.data.type){
          this.event.publish('setRootPage');
      }*/
  }

  ionViewWillEnter(){
     this.mySelectedIndex = this.navParams.get("tabIndex") || 0;
  }

}