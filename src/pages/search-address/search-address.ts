import { Component } from '@angular/core';
import { NavController, NavParams,AlertController,Events} from 'ionic-angular';

/*
  Generated class for the SearchAddress page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-search-address',
  templateUrl: 'search-address.html'
})
export class SearchAddressPage {

  public myInput:any;

  public placeSearch:any;

  public items:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public alertCtrl:AlertController,public events:Events) {

  }

  ionViewDidLoad() {
    AMap.service(["AMap.PlaceSearch"], ()=>{
        this.placeSearch = new AMap.PlaceSearch({ //构造地点查询类
            pageSize: 8,
            pageIndex: 1,
            city: "027"
        });
    });
  }

  onInput(event){
    this.placeSearch.search(event.target.value, (status, result)=> {
      if(result.poiList&&result.poiList.pois){
        debugger;
        this.items = result.poiList.pois;
      }
    });
  }

  itemSelected(item){
    this.events.publish('address:selected', item);
    this.navCtrl.pop();
  }

}
