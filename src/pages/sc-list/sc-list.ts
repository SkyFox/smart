import { Component } from '@angular/core';
import { NavController, NavParams,ActionSheetController } from 'ionic-angular';
import {StationDetailPage} from './../station-detail/station-detail';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import { MyGlobals } from './../../providers/my-globals';
import { MyStoarge } from './../../providers/my-stoarge';


/*
  Generated class for the ScList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-sc-list',
  templateUrl: 'sc-list.html'
})
export class ScListPage {
  public items:any[];
  public stations=[];
  public userInfo:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public actionSheetCtrl: ActionSheetController,
  public myHttp: MyHttp,
    public myTips:MyTips,
    public myGlobals:MyGlobals,public myStoarge: MyStoarge,) {
        this.userInfo = this.myStoarge.getObject("user_info");
        this.getStations('027',this.userInfo&&this.userInfo.mobile||'');
    }

  ionViewDidLoad() {
     //this.getStations();
  }  

  

  goToHome(){
     this.navCtrl.pop();
  }

  itemSelected(station){
    this.navCtrl.push(StationDetailPage,station);
  }

  presentActionSheet(event:Event,station:any) {
    event.stopPropagation();
   let actionSheet = this.actionSheetCtrl.create({
     buttons: [
       {
         text: '高德地图',
         handler: () => {
            let from = this.myGlobals.getMyPosition()[0]+","+this.myGlobals.getMyPosition()[1];
            let to = station.longitude+","+station.latitude;
            console.log(from);
              var navUrl = "http://uri.amap.com/navigation?from="+from+",startpoint&to="+to+",endpoint&mode=car&policy=1&src=mypage&coordinate=gaode&callnative=1";
            window.location.href=navUrl;
         }
       },
       {
         text: '取消',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });
   actionSheet.present();
 }

 getStations(cityCode,mobile){
        this.myHttp.post('device/findStations',{'cityCode':cityCode,'mobile':mobile}).subscribe(
          data =>{

            if(data.msg){
              this.myTips.presentToast(data.msg,3000,"bottom")
            }else{
              this.stations= data;
              this.stations.forEach((station)=>{
                  let lnglat = new AMap.LngLat(this.myGlobals.getMyPosition()[0], this.myGlobals.getMyPosition()[1]);
                  let distant = Math.floor(lnglat.distance([station.longitude, station.latitude])); 
                  station.distant = distant;
              });
            }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",3000,"bottom")
          } 
      )
    }

}
