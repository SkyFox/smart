import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';

/*
  Generated class for the OrderComment page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-order-comment',
  templateUrl: 'order-comment.html'
})
export class OrderCommentPage {


  public imgs={
    'selected' : './assets/img/btn-my-star-select.png',
    'unSelected' : './assets/img/btn-my-star-normal.png'
  };


  public imgPaths=[
    './assets/img/btn-my-star-normal.png','./assets/img/btn-my-star-normal.png',
    './assets/img/btn-my-star-normal.png','./assets/img/btn-my-star-normal.png',
    './assets/img/btn-my-star-normal.png'
  ];

  public rateLevel:any;

  public orderDetail:any;

   public userInfo:any;

   public content:string;

   public disableBtn=null;

   public callBack:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public myTips: MyTips,
    public myStoarge: MyStoarge,
    public myHttp: MyHttp,
    public alertCtrl : AlertController,) {
     this.userInfo = this.myStoarge.getObject("user_info");

     this.orderDetail = navParams.get("order")||'';

     this.callBack    = navParams.get('callBack')||'';
  }

  ionViewDidLoad() {

  }

  selectStar(rate:any){
      this.rateLevel = rate;
      for(var i=0;i<this.imgPaths.length;i++){
          if(i<rate){
            this.imgPaths[i] = './assets/img/btn-my-star-select.png';
          }else{
            this.imgPaths[i] = './assets/img/btn-my-star-normal.png';
          }
      }
  }

  comment(){
      this.myHttp.post('other/inputAppraise',{'mobile':this.userInfo.mobile,'orderCode':this.orderDetail.order_code||this.orderDetail.orderId,
    'grade':this.rateLevel,'content':this.content}).subscribe(
      data =>{
          if(data.sucess===true){
            let confirm = this.alertCtrl.create({
              title: '',
              message: '订单评价成功',
              buttons: [
                  {
                  text: '确定',
                  handler: () => {
                      if(this.callBack){
                          this.callBack().then(()=>{
                              this.navCtrl.pop();
                          });
                      }else{
                          this.navCtrl.pop();
                      }
                    }
                  }
              ]
            });
            confirm.present();
          }else{
            this.myTips.presentToast(data.msg,2000,"bottom")
          }
      },
      error =>{
          console.log(error);
          this.myTips.presentToast("服务器异常！",2000,"bottom")
      } 
    )
  }

}
