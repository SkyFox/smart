import { Component } from '@angular/core';
import { NavController, NavParams,ActionSheetController } from 'ionic-angular';
import { MyGlobals } from './../../providers/my-globals';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';

/*
  Generated class for the StationDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-station-detail',
  templateUrl: 'station-detail.html'
})
export class StationDetailPage {

  selectedSeg:string='station';

  public payTypes=[];

  public isShowHalf = false;

  public stars:any;

  public emptyStars:any;

  station:any;

  public userInfo:any;

  public carTypes=[];

  public officeTime:any;

  public weekTime:any;

  public piles=[];

  public comments:any;



  constructor(public navCtrl: NavController, public navParams: NavParams,
  public actionSheetCtrl: ActionSheetController,
  public myGlobals:MyGlobals,
  public myHttp:MyHttp,
  public myTips:MyTips,
  public myStoarge: MyStoarge,) {
      this.userInfo = this.myStoarge.getObject("user_info");
      this.station = this.navParams.data;

      if(this.station.pay_type){
        this.payTypes = this.station.pay_type.split(",");
      }

      if(this.station.business_hours){
        let times = this.station.business_hours.split("|");
        this.officeTime = times[0];
        this.weekTime = times[1]
      }

      if(this.station.cartype_desc){
        this.carTypes = this.station.cartype_desc.split(',');
      }

      this.initRates();
      this.findDevices(this.station.id);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StationDetailPage');
    this.getStationComments();
  }

  initRates(){
        this.stars = new Array(Math.floor(this.station.appraise_grade));
        this.emptyStars = new Array(5-this.stars.length);
        if(parseInt(this.station.appraise_grade)!=this.station.appraise_grade){
            this.isShowHalf = false;
        }
  }

  findDevices(stationId:any){
       this.myHttp.post('device/findDevices',{'stationId' : stationId}).subscribe(
          data =>{
              if(data.hasOwnProperty("msg")){
                this.myTips.presentToast(data.msg,3000,"bottom");
              }else{
                this.piles = data;
              }
          },
          error =>{
              this.myTips.presentToast("服务器异常！",3000,"bottom")
          } 
      )
  }

  addFav(){
    if(this.station.collect!=1){

         this.myHttp.post('other/insertCollect',{'stationId' : this.station.id , 'mobile' : this.userInfo.mobile}).subscribe(
          data =>{
              if(data.sucess){
                 this.station.collect=1;
                 this.myTips.presentToast("收藏成功",3000,"bottom");
              }else{
                this.myTips.presentToast(data.msg,3000,"bottom");
              }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",3000,"bottom")
          } 
      )

    }else{
        
        this.myHttp.post('other/deleteCollect',{'stationIds' : this.station.id , 'mobile' : this.userInfo.mobile}).subscribe(
          data =>{
              if(data.sucess){
                this.station.collect=0;
                 this.myTips.presentToast("取消收藏成功",3000,"bottom");
              }else{
                this.myTips.presentToast(data.msg,3000,"bottom");
              }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",3000,"bottom")
          } 
      )
    }

     
  }

  presentActionSheet(event: Event) {
    event.stopPropagation();
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: '高德地图',
          handler: () => {
            let from = this.myGlobals.getMyPosition()[0]+","+this.myGlobals.getMyPosition()[1];
            let to = this.station.longitude+","+this.station.latitude;
            window.location.href = "http://uri.amap.com/navigation?from="+from+",startpoint&to="+to+",endpoint&mode=car&policy=1&src=mypage&coordinate=gaode&callnative=1"
          }
        },
        {
          text: '取消',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  getStationComments(){
      this.myHttp.post('other/findAppraises',{'stationId' : this.station.id , 'startrow' : 0,'rownum' : 10}).subscribe(
          data =>{
              if(data.hasOwnProperty("msg")){
                 this.myTips.presentToast(data.msg,3000,"bottom")
              }else{
                  this.comments = data;
              }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",3000,"bottom")
          } 
      )
  }

}
