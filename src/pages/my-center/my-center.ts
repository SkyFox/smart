import { Component } from '@angular/core';
import { NavController, NavParams,AlertController ,App,Events} from 'ionic-angular';
import {MyMoneyPage} from './../my-money/my-money';
import {MySettingsPage} from './../my-settings/my-settings';
import { MyStoarge } from './../../providers/my-stoarge';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import {OrderCenterPage} from './../order-center/order-center';
import {HelpCenterPage} from './../help-center/help-center';
import {FeedbackPage} from './../feedback/feedback';MessageCenterPage
import {MessageCenterPage} from './../message-center/message-center';
import {FavoritePage} from './../favorite/favorite';
import {LoginPage} from './../login/login';
import {TabsPage} from './../tabs/tabs';

/*
  Generated class for the MyCenter page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-my-center',
  templateUrl: 'my-center.html'
})
export class MyCenterPage {

  public isCharging=0;

  public userInfo={
    mobile : '',
    myMoney : ''
  };
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public myStoarge: MyStoarge,
  public myHttp: MyHttp,
  public myTips: MyTips,
  public alertCtrl : AlertController,
  public app:App,
  public event:Events) {
    this.userInfo = this.myStoarge.getObject("user_info");
    if(this.userInfo){
      this.checkCharingStatus();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyCenterPage');
  }
  ionViewDidEnter(){
    this.getAccountMoney();
  }

  ionViewWillEnter(){
    
  }

  ionViewCanEnter() :boolean | Promise<boolean>{
    if(this.userInfo){
        return true;
    }else{
      return new Promise((resolve: any, reject: any) => {
        let alert = this.alertCtrl.create({
          title: '请先登录',
          message: '登录后可查看个人信息.'
        });
        alert.addButton({ text: '取消', handler:()=>{
            this.app.getRootNav().setRoot(TabsPage,{"tabIndex":0});
            reject();
        }  });
        alert.addButton({ text: '登录', handler:()=>{
            console.log("----------------------------");
            this.app.getRootNav().setRoot(LoginPage);
            resolve();
        }});

        alert.present();
      });
    }
  }

  getAccountMoney(){
    this.myHttp.post('service/findAccountMoney',{'mobile':this.userInfo.mobile}).subscribe(
      data =>{
            this.userInfo.myMoney = data.account_money;
      },
      error =>{
          console.log(error);
          this.myTips.presentToast("服务器异常！",2000,"bottom")
      } 
    )
  }

  goToMyMoney(){
      this.navCtrl.push(MyMoneyPage);
  }

  goToSMySettings(){
    this.navCtrl.push(MySettingsPage);
  }

  goToOrders(){
    this.navCtrl.push(OrderCenterPage);
  }

  goToHelpCenter(){
    this.navCtrl.push(HelpCenterPage);
  }
  goToFeedback(){
    this.navCtrl.push(FeedbackPage);
  }
  goToMessageCenter(){
    this.navCtrl.push(MessageCenterPage);
  }

  goToFavorites(){
    this.navCtrl.push(FavoritePage);
  }

  goToChargingOrders(){
     if(this.isCharging==1){
        this.navCtrl.push(OrderCenterPage);
     }
     return;
  }

  goToCommentOrders(){
     this.navCtrl.push(OrderCenterPage,{"index":3});
  }

  checkCharingStatus(){
      this.myHttp.post('service/findProcessOrder',{'mobile' : this.userInfo.mobile}).subscribe(
          data =>{
              if(data.hasOwnProperty("order_code")){
                  this.isCharging = 1;
              }else{
                  this.isCharging = 0;
              }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",3000,"bottom")
          } 
      )
   }

}
