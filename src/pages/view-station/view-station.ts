import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ActionSheetController } from 'ionic-angular';
import {StationDetailPage} from './../station-detail/station-detail';
import { MyGlobals } from './../../providers/my-globals';
/*
  Generated class for the ViewStation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-view-station',
  templateUrl: 'view-station.html'
})
export class ViewStationPage {

  public myPosition:any;

  public distant:any;

  public stationDetail={
      name : '',
      quickporttotal : '',
      quickporttotal_available : '',
      slowporttotal : '',
      slowporttotal_available : '',
      address : '',
      pay_type : '',
      longitude : '',
      latitude : ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public actionSheetCtrl: ActionSheetController,
    public myGlobals:MyGlobals) { }

  ionViewDidLoad() {
    this.stationDetail = this.navParams.data.station;
    let lnglat = new AMap.LngLat(this.myGlobals.getMyPosition()[0], this.myGlobals.getMyPosition()[1]);
    this.distant = Math.floor(lnglat.distance([this.stationDetail.longitude, this.stationDetail.latitude])); 

  }

  ionViewWillEnter(){
      let modal = document.getElementsByTagName('ion-modal')[0];
      let modalWrapper = document.getElementsByClassName('modal-wrapper')[0];
      modal.classList.add("spec-ion-modal"); 
      modalWrapper.classList.add('spec-modal-wrapper');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  presentActionSheet(event: Event) {
    event.stopPropagation();
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: '高德地图',
          handler: () => {
            let from = this.myGlobals.getMyPosition()[0]+","+this.myGlobals.getMyPosition()[1];
            let to = this.stationDetail.longitude+","+this.stationDetail.latitude;
            let navUrl = "http://uri.amap.com/navigation?from="+from+",startpoint&to="+to+",endpoint&mode=car&policy=1&src=mypage&coordinate=gaode&callnative=1";
             window.location.href = navUrl;
          }
        },
        {
          text: '取消',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  itemSelected(){
      this.navCtrl.push(StationDetailPage,this.stationDetail); 
  }

}
