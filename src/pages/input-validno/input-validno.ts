import { Component } from '@angular/core';
import { NavController, NavParams ,ModalController,AlertController} from 'ionic-angular';
import {VaildSuccessPage} from './../vaild-success/vaild-success';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';

/*
  Generated class for the InputValidno page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-input-validno',
  templateUrl: 'input-validno.html'
})
export class InputValidnoPage {

  public userInfo:any;
  public serialNumber:any;
  public portCode:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public modalCtrl : ModalController,
  public myHttp: MyHttp,
  public alertCtrl: AlertController,
  public tipCtrl:MyTips,
  public myStoarge:MyStoarge) {}

  ionViewDidLoad() {
    this.userInfo = this.myStoarge.getObject("user_info");
    console.log('ionViewDidLoad InputValidnoPage');
  }

  validBtn(){
     let myPresent= this.tipCtrl.presentLoading("ios-small","认证查询中...");
     myPresent.present();
     this.myHttp.post('device/verifyDevice',
     {"portCode":this.portCode,"loginId":this.userInfo.mobile}).subscribe(
      data =>{
        if(data.sucess==true){
           this.serialNumber = data.msg;
           this.queryVerifyResult(1,myPresent);
        }else{
             let alert = this.alertCtrl.create({
                  title: '',
                  subTitle: data.msg,
                  buttons: ['确定']
                });
                alert.present();
                myPresent.dismiss();
        }
      },
      error =>{ 
        myPresent.dismissAll();
      }
    )
    
  }

  queryVerifyResult(status:any,myPresent:any){
      if(status==1){
          this.myHttp.post('device/verifyDeviceResult',
           {"serialNumber":this.serialNumber}).subscribe(
            data =>{
              if(data.flag==1){
                    myPresent.dismiss();
                    let modal = this.modalCtrl.create(VaildSuccessPage,data);
                        modal.present();
                    
                    modal.onDidDismiss(()=>{
                        this.navCtrl.pop();
                    });
                    
                    this.userInfo.portCode = this.portCode;
                    this.myStoarge.setObject("user_info" ,this.userInfo);
                    this.getChargingResult();
              }else if(data.flag==2){
                    this.queryVerifyResult(1,myPresent);
              }else if(data.flag==0){
                  myPresent.dismiss();
                  if(data.msg==1){
                    this.tipCtrl.presentToast('此设备尚未插枪',2000,"bottom");
                  }else if(data.msg==2){
                    this.tipCtrl.presentToast('设备检测失败',2000,"bottom");
                  }
              }else{
                  myPresent.dismiss();
                  let alert = this.alertCtrl.create({
                        title: '',
                        subTitle: data.msg,
                        buttons: ['确定']
                      });
                      alert.present();
              }
                console.log(data);
            },
            error => console.log(error)
          )
      }
  }

  getChargingResult(){
    this.myHttp.post('device/startCharingResult',{'portCode':this.portCode,"loginId":this.userInfo.mobile}).subscribe(
        data =>{
          if(data.flag == 2){
              this.getChargingResult();
          }
        },
        error =>{
            console.log(error);
        } 
    )
  }

}
