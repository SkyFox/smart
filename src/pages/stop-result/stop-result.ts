import { Component } from '@angular/core';
import { NavController, NavParams,ViewController} from 'ionic-angular';
import {OrderCommentPage} from './../order-comment/order-comment';

/*
  Generated class for the StopResult page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-stop-result',
  templateUrl: 'stop-result.html'
})
export class StopResultPage {


  public orderInfo:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public viewCtrl: ViewController) {
    console.log(this.navParams);
    this.orderInfo = this.navParams.data;
    console.log('ionViewDidLoad StopResultPage');
  }

  ionViewDidLoad() {
  }

  goToComment(){
      this.navCtrl.push(OrderCommentPage,{order:this.orderInfo});
  }

  goToOrders(){
      this.navCtrl.push(OrderCommentPage,{order:this.orderInfo});
  }

  dismiss(){
      this.viewCtrl.dismiss();
  }

}
