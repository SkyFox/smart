import { Component } from '@angular/core';
import { NavController, NavParams ,ViewController,Events, App} from 'ionic-angular';
import { HomePage } from './../home/home';

/*
  Generated class for the VaildSuccess page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-vaild-success',
  templateUrl: 'vaild-success.html'
})
export class VaildSuccessPage {

  public obj:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public viewCtrl: ViewController,
  public event :Events,
  public appCtrl: App) {
      this.obj=JSON.parse(navParams.data.msg);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VaildSuccessPage');
  }

  dismiss() {
     this.viewCtrl.dismiss();
  }

}
