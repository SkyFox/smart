import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from './../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { MyCenterPage } from './../pages/my-center/my-center';
import { LoginPage } from './../pages/login/login';
import { QRCodePage } from './../pages/qr-code/qr-code';
import { RegisterPage } from './../pages/register/register';

import { ForgotPwdPage } from './../pages/forgot-pwd/forgot-pwd';
import {ScListPage} from './../pages/sc-list/sc-list';
import {ViewStationPage} from './../pages/view-station/view-station';
import {StationDetailPage} from './../pages/station-detail/station-detail';
import {SearchAddressPage} from './../pages/search-address/search-address';
import {SearchCityPage} from './../pages/search-city/search-city';
import {InputValidnoPage} from './../pages/input-validno/input-validno';
import {VaildSuccessPage} from './../pages/vaild-success/vaild-success';
import {ChargingStatusPage} from './../pages/charging-status/charging-status';
import {MyMoneyPage} from './../pages/my-money/my-money';
import {RechargePage} from './../pages/recharge/recharge';
import {MySettingsPage} from './../pages/my-settings/my-settings';
import {PersonInfoPage} from './../pages/person-info/person-info';
import {TutorialPage} from './../pages/tutorial/tutorial';
import {StopResultPage} from './../pages/stop-result/stop-result';
import {AboutUsPage} from './../pages/about-us/about-us';
import {OrderCenterPage} from './../pages/order-center/order-center';
import {OrderCommentPage} from './../pages/order-comment/order-comment';
import {HelpCenterPage} from './../pages/help-center/help-center';
import {FeedbackPage} from './../pages/feedback/feedback';
import {MessageCenterPage} from './../pages/message-center/message-center';
import {TransactionDetailsPage} from './../pages/transaction-details/transaction-details';
import {MessageDetailPage} from './../pages/message-detail/message-detail';
import {ChangePasswordPage} from './../pages/change-password/change-password';
import {ServicesPage} from './../pages/services/services';
import {FavoritePage} from './../pages/favorite/favorite';
import {RegNotesPage} from './../pages/reg-notes/reg-notes';
import {PayNotesPage} from './../pages/pay-notes/pay-notes';
import {UsageNotesPage} from './../pages/usage-notes/usage-notes';
import {UpdateUserPage} from './../pages/update-user/update-user';

import { MyHttp } from './../providers/my-http';
import { MyStoarge } from './../providers/my-stoarge';
import { MyTips } from './../providers/my-tips';
import { MyGlobals } from './../providers/my-globals';




@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    HomePage,
    MyCenterPage,
    LoginPage,
    QRCodePage,
    RegisterPage,
    ForgotPwdPage,
    ScListPage,
    ViewStationPage,
    StationDetailPage,
    SearchAddressPage,
    SearchCityPage,
    InputValidnoPage,
    VaildSuccessPage,
    ChargingStatusPage,
    MyMoneyPage,
    RechargePage,
    MySettingsPage,
    PersonInfoPage,
    TutorialPage,
    StopResultPage,
    AboutUsPage,
    OrderCenterPage,
    OrderCommentPage,
    HelpCenterPage,
    FeedbackPage,
    MessageCenterPage,
    TransactionDetailsPage,
    MessageDetailPage,
    ChangePasswordPage,
    ServicesPage,
    FavoritePage,
    RegNotesPage,
    PayNotesPage,
    UsageNotesPage,
    UpdateUserPage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      iconMode: 'ios',
      mode: 'ios',
      tabsPlacement: 'bottom',
      pageTransition: 'ios',
      tabsHideOnSubPages : true
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    HomePage,
    MyCenterPage,
    LoginPage,
    QRCodePage,
    RegisterPage,
    ForgotPwdPage,
    ScListPage,
    ViewStationPage,
    StationDetailPage,
    SearchAddressPage,
    SearchCityPage,
    InputValidnoPage,
    VaildSuccessPage,
    ChargingStatusPage,
    MyMoneyPage,
    RechargePage,
    MySettingsPage,
    PersonInfoPage,
    TutorialPage,
    StopResultPage,
    AboutUsPage,
    OrderCenterPage,
    OrderCommentPage,
    HelpCenterPage,
    FeedbackPage,
    MessageCenterPage,
    TransactionDetailsPage,
    MessageDetailPage,
    ChangePasswordPage,
    ServicesPage,
    FavoritePage,
    RegNotesPage,
    PayNotesPage,
    UsageNotesPage,
    UpdateUserPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler},MyHttp,MyStoarge,MyTips,MyGlobals]
})
export class AppModule {}
