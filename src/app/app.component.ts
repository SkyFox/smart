import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { TabsPage } from './../pages/tabs/tabs';
import { LoginPage } from './../pages/login/login';
import {TutorialPage} from './../pages/tutorial/tutorial';
import {HomePage} from './../pages/home/home';

import { MyStoarge } from './../providers/my-stoarge';




@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage:any;
  
  constructor(platform: Platform,myStoarge: MyStoarge) {
    platform.ready().then(() => {
      console.log('-----------------app loaded---------------------');
      StatusBar.styleDefault();
      Splashscreen.hide();
      // 判断是否登录
      let userInfo = myStoarge.getObject("user_info");
      let tutorial = myStoarge.getObject("hasSeenTutorial");
      if(userInfo||tutorial){
        this.rootPage = TabsPage;
      }else{
          this.rootPage = TutorialPage;
      }
    });
  }
}
